import 'dart:async';

import 'package:analyzer/dart/element/element.dart';
import 'package:analyzer/dart/element/type.dart';
import 'package:analyzer/dart/element/visitor.dart';
import 'package:build/src/builder/build_step.dart';
import 'package:source_gen/source_gen.dart';
import 'package:app_model/app_model.dart';

class AppModelGenerator extends GeneratorForAnnotation<Entity> {
  @override
  FutureOr<String> generateForAnnotatedElement(
      Element element, ConstantReader annotation, BuildStep buildStep) {
    return generateEntitySource(element);
  }

  String generateEntitySource(Element element) {
    final visitor = ModelVisitor();
    element.visitChildren(visitor);
    final sourceBuilder = StringBuffer();
    // Class name
    sourceBuilder.writeln("class ${visitor.className}Entity extends ${visitor.className} {");

    //Constructor
    sourceBuilder.writeln("${visitor.className}Entity ({");
    for (String propertyName in visitor.fields.keys){
      sourceBuilder.writeln("$propertyName,");
    }
    sourceBuilder.writeln("}): super (");
    for (String propertyName in visitor.fields.keys){
      sourceBuilder.writeln("$propertyName,");
    }
    sourceBuilder.writeln(");");

    sourceBuilder.writeln("");

    //Constant entity
    sourceBuilder.writeln("static const TABLE_NAME = '${visitor.className.toString().toLowerCase()}';");
    var i = 0;
    for (String propertyName in visitor.fields.keys){
      if (i == 0) {
        sourceBuilder.writeln("static const COLUMN_${propertyName.toUpperCase()} = '_$propertyName';");
      } else {
        sourceBuilder.writeln("static const COLUMN_${propertyName.toUpperCase()} = '$propertyName';");
      }
      i++;
    }

    sourceBuilder.writeln("");

    //toMap Function
    sourceBuilder.writeln("Map<String, dynamic> toMap() {");
    sourceBuilder.writeln("return {");
    for (String propertyName in visitor.fields.keys){
      sourceBuilder.writeln("COLUMN_${propertyName.toUpperCase()} : $propertyName,");
    }
    sourceBuilder.writeln("};");
    sourceBuilder.writeln("}");

    sourceBuilder.writeln("");

    //fromMap Function
    sourceBuilder.writeln("${visitor.className}Entity fromMap(Map<String, dynamic> map) {");
    sourceBuilder.writeln("return ${visitor.className}Entity(");
    for (String propertyName in visitor.fields.keys){
      if (visitor.fields[propertyName].toString() == 'bool' || visitor.fields[propertyName].toString() == 'bool?') {
        sourceBuilder.writeln(
            "$propertyName: map[COLUMN_${propertyName.toUpperCase()}] == 1 ? true : false,");
      } else {
        sourceBuilder.writeln(
            "$propertyName: map[COLUMN_${propertyName.toUpperCase()}],");
      }
    }
    sourceBuilder.writeln(");");
    sourceBuilder.writeln("}");

    sourceBuilder.writeln("}");

    return sourceBuilder.toString();
  }
}

class ModelVisitor extends SimpleElementVisitor {
  late DartType className;
  Map<String, DartType> fields = Map();

  @override
  visitConstructorElement(ConstructorElement element) {
    className = element.type.returnType;
    return super.visitConstructorElement(element);
  }

  @override
  visitFieldElement(FieldElement element) {
    fields[element.name] = element.type;

    return super.visitFieldElement(element);
  }
}
