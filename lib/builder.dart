import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';

import 'src/generator.dart';


Builder appModel(BuilderOptions options) => SharedPartBuilder([AppModelGenerator()],'app_model');